import Styles from './EmployeeCard.module.sass';
import { Card, Button } from 'react-bootstrap';

const formatDate = (date) => {
    var month = date.getMonth() + 1;
    month = ("0" + month).slice(-2);
    return `${date.getDate()}/${month}/${date.getFullYear()}`;
}

const formatDateAndHour = (date) => {
    var month = date.getMonth() + 1;
    month = ("0" + month).slice(-2);
    return `${date.getDate()}/${month}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
}

const idTypeDictionary = {
    "CC": "cédula",
    "CE": "cédula de extranjería",
    "PA": "pasaporte",
    "RC": "registro civil",
    "TI": "tarjeta de identidad"
}

const areaDictionary = {
    "Administration": "Administración",
    "Finance": "Financiera",
    "Sales": "Compras",
    "Infrastructure": "Infraestructura",
    "Operation": "Operación",
    "Human Talent": "Talento Humano",
    "Various Services": "Servicios Varios"
}

export default function EmployeeCard({ data: {
    _id,
    lastName,
    secondLastName,
    firstName,
    otherNames,
    country,
    idType,
    idNumber,
    admissionDate,
    email,
    area,
    createdAt,
    state
}, handleEdit, handleDelete }) {
    const nameString = `${firstName} ${otherNames} ${lastName} ${secondLastName}`;
    const areaString = areaDictionary[area];
    const idTypeString = idTypeDictionary[idType];
    const admissionDateString = formatDate(new Date(admissionDate));
    const createdAtString = formatDateAndHour(new Date(createdAt));
    const stateString = state ? "activo" : "inactivo";
    return (
        <div>
            <Card className="d-flex flex-column">
                <Card.Body className="d-flex flex-row align-middle p-1">
                    <Card.Body className="col-md-6 text-right d-flex flex-column justify-content-center">
                        <Card.Title as="h2">{nameString || "n/a"}</Card.Title>
                        <Card.Text>{areaString || "n/a"}</Card.Text>
                    </Card.Body>
                    <Card.Body className="d-flex flex-row flex-wrap ">
                        <Card.Body className="col-md-6 p-2">
                            <Card.Title as="h5">Tipo de Identificación</Card.Title>
                            <Card.Text>{idTypeString || "n/a"}</Card.Text>
                        </Card.Body>
                        <Card.Body className="col-md-6 p-2">
                            <Card.Title as="h5">Número de Identificación</Card.Title>
                            <Card.Text>{idNumber || "n/a"}</Card.Text>
                        </Card.Body>
                        <Card.Body className="col-md-6 p-2">
                            <Card.Title as="h5">Email</Card.Title>
                            <Card.Text>{email || "n/a"}</Card.Text>
                        </Card.Body>
                        <Card.Body className="col-md-6 p-2">
                            <Card.Title as="h5">País</Card.Title>
                            <Card.Text>{country || "n/a"}</Card.Text>
                        </Card.Body>
                        <Card.Body className="col-md-6 p-2">
                            <Card.Title as="h5">Fecha de Entrada</Card.Title>
                            <Card.Text>{admissionDateString || "n/a"}</Card.Text>
                        </Card.Body>
                        <Card.Body className="col-md-6 p-2">
                            <Card.Title as="h5">Tiempo de Registro</Card.Title>
                            <Card.Text>{createdAtString || "n/a"}</Card.Text>
                        </Card.Body>
                        <Card.Body className="col-md-6 p-2">
                            <Card.Title as="h5">Estado</Card.Title>
                            <Card.Text>{stateString || "n/a"}</Card.Text>
                        </Card.Body>
                    </Card.Body>
                </Card.Body>
                <Card.Body className="d-flex flex-row justify-content-end p-1">
                    <Button variant="primary" className="m-1" onClick={(event) => handleEdit(event, _id)}>Editar</Button>
                    <Button variant="primary" className="m-1" onClick={(event) => handleDelete(event, _id)}>Eliminar</Button>
                </Card.Body>
            </Card>
        </div>
    );
}