import { Form, Button, Container } from 'react-bootstrap';

const formatDate = (date) => {
    var month = date.getUTCMonth() + 1;
    month = ("0" + month).slice(-2);
    var day = date.getUTCDate();
    day = ("0" + day).slice(-2);
    return `${date.getUTCFullYear()}-${month}-${day}`;
}

export default function EmployeesForm({ data: {
    lastName,
    secondLastName,
    firstName,
    otherNames,
    country,
    idType,
    idNumber,
    admissionDate,
    area,
    state,
}, handleInputChange, handleSubmit }) {
    const currentDate = formatDate(new Date());
    const oneMonthAgoDate = formatDate(new Date(new Date() - 2628000000));
    const admissionDateString = formatDate(new Date(admissionDate));
    return (
        <div>
            <Container className="d-flex flex-column">
                <Form className="d-flex flex-row flex-wrap">
                    <Form.Group className="col-md-6">
                        <Form.Label>Primer Nombre:</Form.Label>
                        <Form.Control type="text" name="firstName" value={firstName} onChange={handleInputChange} />
                    </Form.Group>
                    <Form.Group className="col-md-6">
                        <Form.Label>Otros Nombres:</Form.Label>
                        <Form.Control type="text" name="otherNames" value={otherNames} onChange={handleInputChange} />
                    </Form.Group>
                    <Form.Group className="col-md-6">
                        <Form.Label>Apellido:</Form.Label>
                        <Form.Control type="text" name="lastName" value={lastName} onChange={handleInputChange} />
                    </Form.Group>
                    <Form.Group className="col-md-6">
                        <Form.Label>Segundo Apellido:</Form.Label>
                        <Form.Control type="text" name="secondLastName" value={secondLastName} onChange={handleInputChange} />
                    </Form.Group>
                    <Form.Group className="col-md-6">
                        <Form.Label>País:</Form.Label>
                        <Form.Control
                            as="select"
                            className="mr-sm-2"
                            id="inlineFormCustomSelect"
                            custom
                            name="country"
                            value={country}
                            onChange={handleInputChange}
                        >
                            <option value="">Ninguno</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Estados Unidos">Estados Unidos</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group className="col-md-6">
                        <Form.Label>Tipo de Identificación:</Form.Label>
                        <Form.Control
                            as="select"
                            className="mr-sm-2"
                            id="inlineFormCustomSelect"
                            custom
                            name="idType"
                            value={idType}
                            onChange={handleInputChange}
                        >
                            <option value="">Ninguno</option>
                            <option value="CC">cédula de ciudadanía</option>
                            <option value="CE">cédula de extranjería</option>
                            <option value="PA">pasaporte</option>
                            <option value="RC">registro civil</option>
                            <option value="TI">tarjeta de identidad</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group className="col-md-6">
                        <Form.Label>Número de Identificación:</Form.Label>
                        <Form.Control type="text" name="idNumber" value={idNumber} onChange={handleInputChange} />
                    </Form.Group>
                    {
                        (admissionDate !== undefined) ?
                            <Form.Group className="col-md-6">
                                <Form.Label>Fecha de Ingreso:</Form.Label>
                                <Form.Control type="date"
                                    name="admissionDate"
                                    min={oneMonthAgoDate}
                                    max={currentDate}
                                    value={admissionDateString}
                                    onChange={handleInputChange}
                                />
                            </Form.Group>
                            :
                            <div />
                    }
                    <Form.Group className="col-md-6">
                        <Form.Label>Área:</Form.Label>
                        <Form.Control
                            as="select"
                            className="mr-sm-2"
                            id="inlineFormCustomSelect"
                            custom
                            name="area"
                            value={area}
                            onChange={handleInputChange}
                        >
                            <option value="">Ninguno</option>
                            <option value="Administration">Administración</option>
                            <option value="Finance">Financiera</option>
                            <option value="Sales">Compras</option>
                            <option value="Infrastructure">Infraestructura</option>
                            <option value="Operation">Operación</option>
                            <option value="Human Talent">Talento Humano</option>
                            <option value="Various Services">Servicios Varios</option>
                        </Form.Control>
                    </Form.Group>
                    {
                        (state !== undefined) ?
                            <Form.Group className="col-md-6">
                                <Form.Label>Activo:</Form.Label>
                                <Form.Check
                                    type="checkbox"
                                    label="Estado"
                                    name="state"
                                    // checked={state}
                                    value={state}
                                    onChange={handleInputChange}
                                />
                            </Form.Group>
                            :
                            <div />
                    }
                </Form>
                <Button onClick={handleSubmit}>Enviar</Button>
            </Container>
        </div>
    );
}

