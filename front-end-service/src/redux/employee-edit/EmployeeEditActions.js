export const editEmployee = (payload) => {
    return {
        type: 'EDIT',
        payload
    };
}