export default (state = {}, action) => {
    switch (action.type) {
        case 'EDIT':
            return { id: action.payload.id };
        default:
            return state;
    }
};