import { combineReducers } from 'redux';
import EmployeeEditReducer from './employee-edit/EmployeeEditReducer.js';

export default combineReducers({ EmployeeEditReducer });