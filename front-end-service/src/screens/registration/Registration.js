import { useState } from "react";
import axios from "axios";
import EmployeeForm from '../../components/employee-form/EmployeeForm.js';
import InformationModal from '../../components/information-modal/InformationModal.js';

const regexDictionary = {
    lastName: /^[a-zA-Z]{0,20}$/,
    secondLastName: /^[a-zA-Z]{0,20}$/,
    firstName: /^[a-zA-Z]{0,20}$/,
    otherNames: /^[a-zA-Z\s]{0,20}$/,
    idNumber: /^[a-zA-Z0-9\-\s]{0,20}$/,
}

export default function Registration() {
    const [modalData, setModalData] = useState({
        title: "",
        message: "",
        show: false
    });
    const [data, setData] = useState({
        lastName: "",
        secondLastName: "",
        firstName: "",
        otherNames: "",
        country: "Colombia",
        idType: "CC",
        idNumber: "",
        admissionDate: "",
        area: "Administration",
    });

    const handleInputChange = (event) => {
        var handleRegex = regexDictionary[event.target.name] || /[\s\S]*/
        if (handleRegex.test(event.target.value)) {
            setData({
                ...data,
                [event.target.name]: event.target.value,
            });
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        axios
            .post("http://localhost:5000/employees", data)
            .then((postData) => {
                const email = postData.data.email;
                setData({
                    ...data,
                    email: email,
                });

                setModalData({
                    title: "Creación usuario",
                    message: `Se creó el usuario exitosamente con el email: ${email}`,
                    show: true
                });
            })
            .catch((error) => {
                console.log(error);
                setModalData({
                    title: "Error",
                    message: "No se pudo crear el usuario",
                    show: true
                });
            });
    };

    const handleModalClose = () => {
        setModalData({
            ...modalData,
            show: false
        });
    }

    return (
        <div>
            <EmployeeForm data={data} handleInputChange={handleInputChange} handleSubmit={handleSubmit} />
            <InformationModal data={modalData} onClose={handleModalClose} />
        </div>
    );
}
