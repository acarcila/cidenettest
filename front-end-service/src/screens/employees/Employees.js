import { useState, useEffect } from "react";
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button } from 'react-bootstrap';
import { editEmployee } from '../../redux/employee-edit/EmployeeEditActions.js';
import axios from "axios";
import EmployeeCard from '../../components/employee-card/EmployeeCard.js';
import EmployeeForm from '../../components/employee-form/EmployeeForm.js';
import WarningModal from '../../components/warning-modal/WarningModal.js';

const regexDictionary = {
    lastName: /^[a-zA-Z]{0,20}$/,
    secondLastName: /^[a-zA-Z]{0,20}$/,
    firstName: /^[a-zA-Z]{0,20}$/,
    otherNames: /^[a-zA-Z\s]{0,20}$/,
    idNumber: /^[a-zA-Z0-9\-\s]{0,20}$/,
}

const generateRequestString = (currentPageNumber, baseString, params) => {
    var finalString = `${baseString}?skip=${10 * (currentPageNumber - 1)}&limit=${10}`;
    for (const key in params) {
        if (params[key] !== "")
            finalString = `${finalString}&${key}=${params[key]}`;
    }
    finalString = finalString.replace(' ', '+');
    return finalString;
}

export default function Employees() {
    const [employees, setEmployees] = useState([]);
    const [pageNumber, setPageNumber] = useState(1);
    const [formToggle, setFormToggle] = useState(false);
    const [modalToggle, setModalToggle] = useState(false);
    const [deleteEmployeeId, setDeleteEmployeeId] = useState("");
    const [searchInfo, setSearchInfo] = useState({
        lastName: "",
        secondLastName: "",
        firstName: "",
        otherNames: "",
        country: "",
        idType: "",
        idNumber: "",
        email: "",
        state: ""
    });
    const history = useHistory();
    const dispatch = useDispatch();
    const baseRequestString = `http://localhost:5000/employees/params`;

    useEffect(() => {
        getEmployees();
    }, []);

    useEffect(() => {

    }, [employees]);

    const getEmployees = () => {
        const requestString = generateRequestString(pageNumber, baseRequestString, searchInfo);
        axios.get(requestString)
            .then(getData => {
                setEmployees(getData.data);
            })
            .catch(error => {
                console.log(error);
                alert("There was a problem");
            });
    }

    const previousPage = () => {
        const page = pageNumber - 1;
        const requestString = generateRequestString(page, baseRequestString, searchInfo);
        if (page >= 1) {
            axios.get(requestString)
                .then(getData => {
                    setPageNumber(page);
                    setEmployees(getData.data);
                    console.log(getData);
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }

    const nextPage = () => {
        const page = pageNumber + 1;
        const requestString = generateRequestString(page, baseRequestString, searchInfo);
        axios.get(requestString)
            .then(getData => {
                if (getData.data.length > 0) {
                    setPageNumber(page);
                    setEmployees(getData.data);
                    console.log(getData);
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    const handleInputChange = (event) => {
        var handleRegex = regexDictionary[event.target.name] || /[\s\S]*/
        if (handleRegex.test(event.target.value)) {
            setSearchInfo({
                ...searchInfo,
                [event.target.name]: event.target.value,
            });
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const requestString = generateRequestString(1, baseRequestString, searchInfo);
        console.log(requestString);
        axios.get(requestString)
            .then(getData => {
                setPageNumber(1);
                setEmployees(getData.data);
                console.log(getData);
            })
            .catch(error => {
                console.log(error);
            });
    }

    const handleCardEdit = (event, id) => {
        dispatch(editEmployee({ id: id }));
        history.push("/edit");
    }

    const handleCardDelete = (event, id) => {
        event.preventDefault();
        setDeleteEmployeeId(id);
        setModalToggle(true);
    }

    const handleDelete = (event) => {
        console.log("eliminar");
        setModalToggle(false);
        axios.delete(`http://localhost:5000/employees/id/${deleteEmployeeId}`)
            .then(getData => {
                getEmployees();
                console.log(getData);
            })
            .catch(error => {
                console.log(error);
            });
    }

    const handleCloseModal = (event) => {
        setModalToggle(false);
    }

    const employeesCards = employees.map((employee, i) => {
        return (
            <div key={i} className="m-2">
                <EmployeeCard
                    data={employee}
                    handleEdit={handleCardEdit}
                    handleDelete={handleCardDelete}
                />
            </div>
        )
    });

    const modalData = { title: "Cuidado", message: "¿Está seguro de que desea eliminar este empleado?", show: modalToggle };
    return (
        <div className="d-flex flex-column align-items-center">
            <WarningModal data={modalData} onClose={handleCloseModal} onDelete={handleDelete} />
            <Button className="align-self-end col-md-1 m-2" onClick={() => setFormToggle(!formToggle)}>{formToggle ? "Cerrar" : "Filtrar"}</Button>
            {
                formToggle ?
                    <div>
                        <EmployeeForm data={searchInfo} handleInputChange={handleInputChange} handleSubmit={handleSubmit} />
                    </div>
                    :
                    <div />
            }
            <div className="d-flex flex-row justify-content-center align-items-center">
                <Button variant="primary" className="m-1" onClick={previousPage}>&lt;</Button>
                <div className="h5 m-2">{pageNumber}</div>
                <Button variant="primary" className="m-1" onClick={nextPage}>&gt;</Button>
            </div>
            <div>
                {employeesCards}
            </div>
            <div className="d-flex flex-row justify-content-center align-items-center">
                <Button variant="primary" className="m-1" onClick={previousPage}>&lt;</Button>
                <div className="h5 m-2">{pageNumber}</div>
                <Button variant="primary" className="m-1" onClick={nextPage}>&gt;</Button>
            </div>
        </div>
    );
}

