import { useState, useEffect } from "react";
import { useSelector } from 'react-redux';
import { Alert } from 'react-bootstrap'
import axios from "axios";
import EmployeeForm from '../../components/employee-form/EmployeeForm.js';
import InformationModal from '../../components/information-modal/InformationModal.js';

const regexDictionary = {
    lastName: /^[a-zA-Z]{0,20}$/,
    secondLastName: /^[a-zA-Z]{0,20}$/,
    firstName: /^[a-zA-Z]{0,20}$/,
    otherNames: /^[a-zA-Z\s]{0,20}$/,
    idNumber: /^[a-zA-Z0-9\-\s]{0,20}$/,
}

export default function Registration() {
    const [updateData, setUpdateData] = useState({});
    const [modalData, setModalData] = useState({
        title: "",
        message: "",
        show: false
    });
    const [currentData, setCurrentData] = useState({
        lastName: "",
        secondLastName: "",
        firstName: "",
        otherNames: "",
        country: "",
        idType: "",
        idNumber: "",
        admissionDate: "",
        area: "",
        state: false,
    });
    var employeeId = useSelector((state) => state.EmployeeEditReducer.id);

    useEffect(() => {
        axios.get(`http://localhost:5000/employees/id/${employeeId}`)
            .then(getData => {
                setCurrentData(getData.data);
            })
            .catch((error) => {
                console.log(error);
                setModalData({
                    title: "Error",
                    message: `No se pudo encontrar el empleado`,
                    show: true
                });
            });
    }, [])

    const handleInputChange = (event) => {
        var handleRegex = regexDictionary[event.target.name] || /[\s\S]*/
        if (handleRegex.test(event.target.value)) {
            setUpdateData({
                ...updateData,
                [event.target.name]: event.target.value,
            });

            setCurrentData({
                ...currentData,
                [event.target.name]: event.target.value,
            });
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const requestBody = {
            ...updateData,
            _id: employeeId
        };
        axios
            .put("http://localhost:5000/employees", requestBody)
            .then((putData) => {
                const email = putData.data.email;
                setCurrentData(putData.data);
                setUpdateData({});
                setModalData({
                    title: "Actualización usuario",
                    message: `Se actualizó el usuario exitosamente con el email: ${email}`,
                    show: true
                });
            })
            .catch((error) => {
                console.log(error);
                setModalData({
                    title: "Error",
                    message: "No se pudo actualizar el usuario",
                    show: true
                });
            });
    };

    const handleModalClose = () => {
        setModalData({
            ...modalData,
            show: false
        });
    }

    var renderData = {}
    for (const key in currentData) {
        renderData[key] = updateData[key] || currentData[key];
    }

    return (
        <div>
            <EmployeeForm data={renderData} handleInputChange={handleInputChange} handleSubmit={handleSubmit} />
            <InformationModal data={modalData} onClose={handleModalClose} />
            <Alert variant="success" className="m-3">El email del empleado es {renderData.email}</Alert>
        </div>
    );
}
