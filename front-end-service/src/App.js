import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import './App.css';
import Registration from './screens/registration/Registration.js';
import Employees from './screens/employees/Employees.js';
import Edit from './screens/edit/Edit.js';

function App() {
  return (
    <Router>
      <Navbar>
        <Navbar.Brand as={Link} to="/">Cidenet</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/registration">Registro</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      {/* <div className="App">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/registration">Registro</Link>
          </li>
        </ul>
      </div> */}

      <Switch>
        <Route path="/edit">
          <Edit />
        </Route>
        <Route path="/registration">
          <Registration />
        </Route>
        <Route path="/">
          <Employees />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
