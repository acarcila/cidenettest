# Pasos para correr este proyecto:
Para descargar el proyecto:
```
git clone https://gitlab.com/acarcila/cidenettest.git

cd cidenettest
```
## Correr el servicio back-end:
Estando en la carpeta `./back-end-service` de este proyecto, crear un archivo con el nombre `config.js` y editarlo con el siguiente contenido
```
export const mongoDBConfig = {
    CONNECTION_STRING: <connectionString>,
    DB_NAME: <databaseName>
}
```
donde `<connectionString>` es la cadena de connexión de la base de datos MongoDb y `<databaseName>` es el nombre de la base de datos.

En este punto, para iniciar el servicio se deben usar los siguientes comandos estando en la carpeta `./back-end-service`:
```
yarn install

yarn start
```
## Correr el servicio front-end:
Estando en la carpeta `./front-end-service` se deben usar los siguientes comandos
```
yarn install

yarn start
```

