import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';

import { mongoDBConfig } from '../../config.js';

export default class Application {
    constructor(port, services) {
        this.app = express();
        this.port = port;
        this.initializeMiddleware();
        this.initializeServices(services);
        this.initializeMongoose();
    }

    initializeMiddleware = () => {
        this.app.use(bodyParser.json());
        this.app.use(cors());
    }

    initializeServices = (services) => {
        services.forEach(service => this.app.use("/", service.getRouter()));
    }

    initializeMongoose = () => {
        mongoose.connect(mongoDBConfig.CONNECTION_STRING, { dbName: mongoDBConfig.DB_NAME, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
        this.mongooseConnection = mongoose.connection;
        this.checkForMongoDBErrors();
        this.checkForMongoDBConnection();
    }

    checkForMongoDBErrors = () => {
        this.mongooseConnection.on('error', error => console.log(error));
    }

    checkForMongoDBConnection = () => {
        this.mongooseConnection.once('open', () => console.log('Connected to mongoDB'));
    }

    start = () => {
        this.app.listen(this.port, () => console.log(`app listening on port ${this.port}`));
    }
}