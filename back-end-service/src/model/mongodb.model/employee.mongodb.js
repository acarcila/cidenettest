import mongoose from 'mongoose';

let employeeSchema = new mongoose.Schema({
    lastName: String,
    secondLastName: String,
    firstName: String,
    otherNames: String,
    country: String,
    idType: String,
    idNumber: String,
    email: String,
    admissionDate: Date,
    area: String,
    state: { type: Boolean, default: true },
}, {
    timestamps: true
});
employeeSchema.path('_id');

export default mongoose.model('employees', employeeSchema);