import mongoose from 'mongoose';

let emailSchema = new mongoose.Schema({
    email: String,
    count: { type: Number, default: 0 }
});
emailSchema.path('_id');

export default mongoose.model('emails', emailSchema);