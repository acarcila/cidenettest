import IEmployeeService from './employee.service.interface.js';

export default class EmployeeService extends IEmployeeService {
    constructor(path, router, employeeController) {
        super();

        this.path = path;
        this.router = router;
        this.employeeController = employeeController;
        this.initializeRoutes();
    }

    getRouter = () => {
        return this.router;
    }

    errorHandlingMiddleware = (error, request, response, next) => {
        response.status(400).send({ error: error.message });
    }

    initializeRoutes = () => {
        this.router.get(this.path.concat("/all"), this.findEmployees);
        this.router.get(this.path.concat("/id/:id"), this.findEmployeesById);
        this.router.get(this.path.concat("/params"), this.findEmployeesByParams);
        this.router.post(this.path.concat(""), this.createEmployee);
        this.router.put(this.path.concat(""), this.updateEmployee);
        this.router.delete(this.path.concat("/id/:id"), this.deleteEmployee);
        this.router.use(this.errorHandlingMiddleware);
    }

    findEmployees = (request, response, next) => {
        this.employeeController.findEmployees(request, response, next);
    }

    findEmployeesById = (request, response, next) => {
        this.employeeController.findEmployeesById(request, response, next);
    }

    findEmployeesByParams = (request, response, next) => {
        this.employeeController.findEmployeesByParams(request, response, next);
    }

    createEmployee = (request, response, next) => {
        this.employeeController.createEmployee(request, response, next);
    }

    updateEmployee = (request, response, next) => {
        this.employeeController.updateEmployee(request, response, next);
    }

    deleteEmployee = (request, response, next) => {
        this.employeeController.deleteEmployee(request, response, next);
    }
}