import IService from "../service.interface.js";

export default class IEmployeeService extends IService {
    constructor() {
        super();
        if (this.constructor == IEmployeeService) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }

    getRouter = () => {
        throw new Error("Method must be implemented.");
    }

    findEmployees = (request, response) => {
        throw new Error("Method must be implemented.");
    }

    findEmployeesByParams = (request, response) => {
        throw new Error("Method must be implemented.");
    }

    createEmployee = (request, response) => {
        throw new Error("Method must be implemented.");
    }

    updateEmployee = (request, response) => {
        throw new Error("Method must be implemented.");
    }

    deleteEmployee = (request, response) => {
        throw new Error("Method must be implemented.");
    }
}