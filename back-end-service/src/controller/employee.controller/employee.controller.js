import IEmployeeController from './employee.controller.interface.js';

export default class EmployeeController extends IEmployeeController {
    constructor(employeeDataController) {
        super();

        this.employeeDataController = employeeDataController;
    }

    findEmployees = (request, response, next) => {
        this.employeeDataController.findAll()
            .then(data => response.status(200).json(data))
            .catch(next);
    }

    findEmployeesById = (request, response, next) => {
        const id = request.params.id;
        this.employeeDataController.findById(id)
            .then(data => response.status(200).json(data))
            .catch(next);
    }

    findEmployeesByParams = (request, response, next) => {
        var params = request.query;
        const pagination = {
            limit: parseInt(params.limit),
            skip: parseInt(params.skip)
        }
        delete params['limit'];
        delete params['skip'];
        this.employeeDataController.findByParams(params, pagination)
            .then(data => response.status(200).json(data))
            .catch(next);
    }

    createEmployee = (request, response, next) => {
        const data = request.body;
        this.employeeDataController.create(data)
            .then(data => response.status(200).json(data))
            .catch(next);
    }

    updateEmployee = (request, response, next) => {
        const data = request.body;
        this.employeeDataController.update(data)
            .then(data => response.status(200).json(data))
            .catch(next);
    }

    deleteEmployee = (request, response, next) => {
        const id = request.params.id;
        this.employeeDataController.deleteById(id)
            .then(data => response.status(200).json(data))
            .catch(next);
    }
}