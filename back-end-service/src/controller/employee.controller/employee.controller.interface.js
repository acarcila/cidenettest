export default class IEmployeeController {
    constructor() {
        if (this.constructor == IEmployeeController) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }

    findEmployees = (request, response) => {
        throw new Error("Method must be implemented.");
    }

    findEmployeesByParams = (request, response) => {
        throw new Error("Method must be implemented.");
    }

    createEmployee = (request, response) => {
        throw new Error("Method must be implemented.");
    }

    updateEmployee = (request, response) => {
        throw new Error("Method must be implemented.");
    }

    deleteEmployee = (request, response) => {
        throw new Error("Method must be implemented.");
    }
}