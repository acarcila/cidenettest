import express from 'express';

import Application from './application/application.js';
import EmployeeService from './api/employee.service/employee.service.js';
import EmployeeController from './controller/employee.controller/employee.controller.js';
import EmployeeDataController from './data.controller/persistence/employee.data.controller/employee.data.controller.js'
import EmailDataController from './data.controller/persistence/email.data.controller/email.data.controller.js';

class Main {
    constructor(port) {
        this.port = port;

        const router = express.Router();
        const employeeDataController = new EmployeeDataController(new EmailDataController());
        this.application = new Application(this.port, [new EmployeeService("/employees", router, new EmployeeController(employeeDataController))]);
    }

    start = () => {
        this.application.start();
    }
}

new Main(5000).start();