import IEmployeeDataController from "./employee.data.controller.interface.js";
import EmployeeModel from '../../../model/mongodb.model/employee.mongodb.js';
import EmailModel from '../email.data.controller/email.data.controller.js';

export default class EmployeeDataController extends IEmployeeDataController {
    constructor(emailDataController) {
        super();

        this.emailDataController = emailDataController;
    }

    findAll = () => {
        return EmployeeModel.find({});
    }

    findById = (id) => {
        return EmployeeModel.findOne({ _id: id });
    }

    create = (data) => {
        const { firstName, lastName, country } = data;
        var email = this.generateEmail(firstName, lastName, country, null);
        return new Promise((resolve, reject) => {
            this.findExistentEmail(email)
                .then(existentEmailData => {
                    var emailData = existentEmailData;

                    if (existentEmailData === null) {
                        this.createNewEmail({ email: email })
                            .then(() => createEmployee())
                            .catch(error => reject(error));
                    }
                    else {
                        email = this.generateEmail(firstName, lastName, country, existentEmailData.count + 1);
                        emailData.count = emailData.count + 1;
                        this.updateEmail(emailData)
                            .then(() => createEmployee())
                            .catch(error => reject(error));
                    }

                    const createEmployee = () => {
                        data.email = email;
                        const employeeModel = new EmployeeModel(data)
                        employeeModel.save()
                            .then(data => resolve(data))
                            .catch(error => reject(error));
                    }
                })
                .catch(error => reject(error));
        });
    }

    update = (data) => {


        return new Promise((resolve, reject) => {
            this.findById(data._id)
                .then(currentEmployeeData => {
                    if ((data.firstName && data.firstName !== currentEmployeeData.firstName) || (data.lastName && data.lastName !== currentEmployeeData.lastName)) {
                        currentEmployeeData.firstName = data.firstName || currentEmployeeData.firstName;
                        currentEmployeeData.lastName = data.lastName || currentEmployeeData.lastName;
                        var email = this.generateEmail(currentEmployeeData.firstName, currentEmployeeData.lastName, currentEmployeeData.country, null);
                        this.findExistentEmail(email)
                            .then(existentEmailData => {
                                var emailData = existentEmailData;

                                if (existentEmailData === null) {
                                    currentEmployeeData.email = email;
                                    this.createNewEmail({ email: email })
                                        .then(() => this.updateEmployee(resolve, reject, data, currentEmployeeData))
                                        .catch(error => reject(error));
                                }
                                else {
                                    email = this.generateEmail(currentEmployeeData.firstName, currentEmployeeData.lastName, currentEmployeeData.country, existentEmailData.count + 1);
                                    currentEmployeeData.email = email;
                                    emailData.count = emailData.count + 1;
                                    this.updateEmail(emailData)
                                        .then(() => this.updateEmployee(resolve, reject, data, currentEmployeeData))
                                        .catch(error => reject(error));
                                }


                            })
                            .catch(error => reject(error));
                    } else {
                        this.updateEmployee(resolve, reject, data, currentEmployeeData);
                    }
                })
                .catch(error => reject(error));
        });

        var employeeModel = new EmployeeModel(data);
        return EmployeeModel.findOneAndUpdate({ _id: employeeModel._id }, employeeModel, { new: true });
    }

    deleteById = (id) => {
        return EmployeeModel.deleteOne({ _id: id });
    }

    findByParams = (params, pagination) => {
        return EmployeeModel.find(params)
            .skip(pagination.skip)
            .limit(pagination.limit);
    }

    generateEmail = (firstName, lastName, country, id) => {
        const x = {
            "Colombia": "cidenet.com.co",
            "Estados Unidos": "cidenet.com.us"
        };

        let domain = x[country];

        //removes accents and spaces
        const firstNameString = firstName.normalize("NFD").replace(/[\u0300-\u036f\-\s]/g, '').toLowerCase();
        const lastNameString = lastName.normalize("NFD").replace(/[\u0300-\u036f\-\s]/g, '').toLowerCase();
        
        const idString = id ? `.${id}` : '';

        return `${firstNameString}.${lastNameString}${idString}@${domain}`;
    }

    findExistentEmail = async (email) => {
        return this.emailDataController.findOneByParams({ email: email });
    }

    createNewEmail = (data) => {
        return this.emailDataController.create(data)
    }

    updateEmail = (data) => {
        return this.emailDataController.update(data)
    }

    updateEmployee = (resolve, reject, updateData, currentEmployeeData) => {
        for (const key in updateData) {
            currentEmployeeData[key] = updateData[key];
        }

        const currentEmployee = new EmployeeModel(currentEmployeeData);
        currentEmployee.save()
            .then(data => resolve(data))
            .catch(error => reject(error));
    }
}