import IEmailDataController from './email.data.controller.interface.js';
import EmailModel from '../../../model/mongodb.model/email.mongodb.js';

export default class EmailDataController extends IEmailDataController {
    constructor() {
        super();
    }

    findAll = () => {
    }

    findById = (id) => {
    }

    create = (data) => {
        const emailModel = new EmailModel(data);
        return emailModel.save();
    }

    update = (data) => {
        var emailModel = new EmailModel(data);
        return EmailModel.findOneAndUpdate({ _id: emailModel._id }, emailModel, { new: true });
    }

    deleteById = (id) => {
    }

    findOneByParams = (params) => {
        return EmailModel.findOne(params);
    }
}